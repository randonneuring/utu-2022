/*global $*/
'use strict';


// External libraries
var DATA_DISPLAY,

    // Global variables
    DATA_LOAD =
        {
            // The relative location of the data files
            dataSource : "data/list-of-services.json",

            // The dictionary array into which all loaded data will be placed
            brevetObjectArray : {},
            response : [],

            // Send an ajax request
            ajaxRequest : function (url, dataType) {

                var debug = true;

                // Send an ajax request to the server - the return value is the
                // response
                return $.ajax({

                    // Input
                    url: url,
                    dataType: dataType,

                    // Settings used in all ajax requests
                    type: "GET",
                    async: true,
                    cache: false,
                    timeout: 30000,

                    // Debugging output on success and error
                    success: function (response) {
                        if (debug) {
                            console.debug("AJAX " + url + " request success");
                            console.debug(response);
                        }
                    },
                    error: function (x, status, error) {
                        // Unauthorized access error
                        if (x.status === 403) {
                            console.log(
                                'HTTP 403: Forbidden (Access is not permitted)'
                            );
                        // Other errors
                        } else {
                            console.log("An error occurred: " + status);
                            console.log("Error: " + error);
                        }
                    },
                });

            },


            createInfoSection : function () {
                var frag = document.createDocumentFragment(), curr_row,
                    curr_col,
                    table_data_container =
                    document.getElementById('smalllist'),
                    btn, nest_row;

                curr_row = document.createElement('div');
                curr_row.classList.add('row');
                curr_row.classList.add('even');

                curr_col = document.createElement('div');
                curr_col.classList.add("col-4");
                curr_col.classList.add("offset-1");
                nest_row = document.createElement('div');
                nest_row.classList.add('row');
                nest_row.classList.add('justify-content-center');
                btn = DATA_LOAD.createButton(
                    "Route - GPS",
                    "btn-dark",
                    'https://ridewithgps.com/routes/37906409',
                    "route"
                );
                nest_row.appendChild(btn);
                curr_col.appendChild(nest_row);
                curr_row.appendChild(curr_col);

                curr_col = document.createElement('div');
                curr_col.classList.add("col-4");
                curr_col.classList.add("offset-2");
                nest_row = document.createElement('div');
                nest_row.classList.add('row');
                nest_row.classList.add('justify-content-center');
                btn = DATA_LOAD.createButton(
                    "UTU Info",
                    "btn-dark",
                    'https://pilgrimbrevet.se/',
                    "home"
                );
                nest_row.appendChild(btn);
                curr_col.appendChild(nest_row);
                curr_row.appendChild(curr_col);

                frag.appendChild(curr_row);
                table_data_container.appendChild(frag);

            },


            createTable : function () {
                var frag = document.createDocumentFragment(), curr_row,
                    curr_col, count_row = 0,
                    table_data_container = document.getElementById('biglist'),
                    i, dayNum, cities, j, btn, k, locations;

                // Loop over each day
                for (i = 0; i < DATA_LOAD.response["2022"].length; i += 1) {
                    console.log(DATA_LOAD.response["2022"][i]);
                    console.log(DATA_LOAD.response["2022"][i].DayNum);

                    dayNum = DATA_LOAD.response["2022"][i].DayNum;
                    cities = DATA_LOAD.response["2022"][i].Cities;

                    // Loop over each city
                    for (j = 0; j < cities.length; j += 1) {

                        // Create a row for each city
                        curr_row = document.createElement('div');
                        count_row += 1;
                        curr_row.classList.add('row');
                        if (count_row % 2 === 0) {
                            curr_row.classList.add('even');
                        } else {
                            curr_row.classList.add('odd');
                        }

                        // Locations column
                        curr_col = document.createElement('div');
                        // curr_col.classList.add("offset-2");
                        // curr_col.classList.add("col-8");
                        curr_col.classList.add("col-12");

                        // Make a button for the city
                        btn = DATA_LOAD.createButton(
                            "Day " + dayNum + " - " + cities[j].Distance +
                                " km - " + cities[j].City,
                            cities[j].Button,
                            cities[j].Url,
                            cities[j].Icon
                        );
                        curr_col.appendChild(btn);

                        // Loop over locations in this city
                        locations = cities[j].Locations;
                        for (k = 0; k < locations.length; k += 1) {

                            btn = DATA_LOAD.createButton(
                                locations[k].Location,
                                locations[k].Button,
                                locations[k].Url,
                                locations[k].Icon
                            );

                            // Place button into column
                            curr_col.appendChild(btn);
                        }

                        curr_row.appendChild(curr_col);

                        console.log("city: " + cities[j].City);

                        // Add the row to the table
                        frag.appendChild(curr_row);

                    }

                }


                table_data_container.appendChild(frag);
            },

            createButton : function (buttonLocation, buttonStyle, buttonUrl,
                 buttonIcon) {

                var btn, icon;

                // Make a button for each location
                btn = document.createElement("a");
                btn.classList.add("btn");
                btn.classList.add(buttonStyle);
                btn.classList.add("text-nowrap");
                btn.classList.add("col-12");
                btn.classList.add("col-sm-6");
                btn.classList.add("col-md-4");
                btn.classList.add("col-lg-3");
                btn.classList.add("col-xl-3");
                btn.classList.add("col-xxl-2");

                // Add a link if specified
                if (buttonUrl !== "") {
                    btn.href = buttonUrl;
                    btn.target = "_blank";
                }

                // Create an icon
                icon = document.createElement("i");
                icon.innerHTML = "&nbsp;" + buttonLocation;
                icon.classList.add("fa");
                icon.classList.add("fa-" + buttonIcon);

                // Place icon and text inside button
                btn.appendChild(icon);

                return btn;
            },

            // Load the data from given json files
            loadBrevetData : function () {

                var debug = true, dataFile, requests = [];

                if (debug) {
                    console.log("loadBrevetData called");
                }

                // Get the data file name
                dataFile = DATA_LOAD.dataSource;

                if (debug) {
                    console.log("dataFile: ", dataFile);
                }

                // Read json data, run as ajax request.
                requests.push(
                    $.when(DATA_LOAD.ajaxRequest(dataFile, "json")
                            ).then(

                        function (response) {
                            // Save output to a global variable
                            DATA_LOAD.response["2022"] = response;
                        }
                    )
                );

                // After data reading is done, send to table creation function
                $.when.apply($, requests).then(
                    function () {
                        DATA_LOAD.createInfoSection();

                        // Create the table containing a list of stops
                        DATA_LOAD.createTable();
                        console.log("done loading");
                    }
                );

            },

        };


// This function fires when the page is ready
$(document).ready(function () {

    var debug = true;

    if (debug) {
        console.log('document is ready');
    }

    // prevent cahing of ajax requests
    $.ajaxSetup({ cache: false });

    // Load the data, then fill the table
    DATA_LOAD.loadBrevetData();

});
